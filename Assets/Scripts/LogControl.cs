﻿using Hive;
using UnityEngine;
using UnityEngine.UI;

public class LogControl : MonoBehaviourWrapper
{
    [SerializeField] private ScrollRect _scrollRect = null;
    [SerializeField] private Text _text = null;

    protected void Awake()
    {
        Application.logMessageReceived += Log;
    }

    public void UpdateContentArea()
    {
        _scrollRect.verticalNormalizedPosition = 0f;
    }

    public void Clear()
    {
        _text.text = string.Empty;
    }

    public void Log(string text)
    {
        _text.text += text + "\n";
        UpdateContentArea();
    }

    public void Log(string text, string stackTrace, LogType type)
    {
        switch (type)
        {
            case LogType.Assert:
                _text.text += string.Format("[ASR] {0}\n", text);
                break;

            case LogType.Log:
                _text.text += string.Format("[INF] {0}\n", text);
                break;

            case LogType.Warning:
                _text.text += string.Format("<color=#FF6E00>[WRN] {0}</color>\n", text);
                break;

            case LogType.Error:
                _text.text += string.Format("<color=red>[ERR] {0}\n{1}</color>\n", text, stackTrace);
                break;

            case LogType.Exception:
                _text.text += string.Format("<color=red>[EXC] {0}\n{1}</color>\n", text, stackTrace);
                break;

            default:
                _text.text += string.Format("[___] {0}\n", text);
                break;
        }
        
        UpdateContentArea();
    }
}

