﻿using UnityEngine;
using Tween;
using Hive;

public class SolidColorShade : DialogShade
{
    private const float TRANSITION_TIME = .4f;

    public tk2dBaseSprite Shade;

    private Color _activeColor;
    private Color _hidedColor;
    private BaseTween _transition;

    private void Awake()
    {
        _hidedColor = _activeColor = Shade.color;
        _hidedColor.a = 0;

        Shade.color = _hidedColor;
    }

    public override bool Show()
    {
        if (!base.Show())
            return false;

        if (_transition != null)
            Shade.StopTween(_transition);
        Shade.gameObject.SetActive(true);
        _transition = new EaseExponentialIn(new ColorTo(_activeColor, TRANSITION_TIME));
        Shade.RunTween(_transition);
        return true;
    }

    public override bool Hide()
    {
        if (!base.Hide())
            return false;

        if (_transition != null)
            Shade.StopTween(_transition);
        _transition = new Sequence(new EaseExponentialIn(new ColorTo(_hidedColor, TRANSITION_TIME)),
                                         new InvokeAction(() => Shade.gameObject.SetActive(false)));
        Shade.RunTween(_transition);
        return true;
    }
}
