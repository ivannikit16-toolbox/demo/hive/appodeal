﻿using Hive.Ads;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class AppodealDemoController : MonoBehaviour
{
    public Text StatusText;

    public void OnShowStaticInterstitial()
    {
        Appodeal.ShowAd(AppodealAdType.Interstitial, AppodealPlacement.StaticInterstitialOnly, (adType, res) => Debug.LogFormat("OnShowStaticInterstitial: {0}", res.ToString()));
    }

    public void OnShowInterstitial()
    {
        Appodeal.ShowAd(AppodealAdType.Interstitial, (adType, res) => Debug.LogFormat("OnShowInterstitial: {0}", res.ToString()));
    }

    public void OnShowRewardedVideo()
    {
        Appodeal.ShowAd(AppodealAdType.RewardedVideo, (adType, res) => Debug.LogFormat("OnShowRewardedVideo: {0}", res.ToString()));
    }

    public void OnShowBanner()
    {
        Appodeal.ShowBanner();
    }

    public void OnHideBanner()
    {
        Appodeal.HideBanner();
    }

    public void OnShowTestScreen()
    {
        Appodeal.ShowTestScreen();
    }


    private void Start()
    {
        StartCoroutine(UpdateStatus());
    }

    private IEnumerator UpdateStatus()
    {
        while (true)
        {
            yield return new WaitForSeconds(1f);

            StatusText.text = string.Format(
                "Status:\n  Interstitial (default): {0}\n  Interstitial (static): {1}\n  Rewarded Video: {2}",
                Appodeal.IsLoaded(AppodealAdType.Interstitial) ? "loaded" : "waiting",
                Appodeal.CanShow(AppodealAdType.Interstitial, AppodealPlacement.StaticInterstitialOnly) ? "loaded" : "waiting",
                Appodeal.IsLoaded(AppodealAdType.RewardedVideo) ? "loaded" : "waiting"
                );
        }
    }
}
