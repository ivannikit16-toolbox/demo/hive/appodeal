﻿using Hive;
using Hive.Ads;
using UnityEngine;

public class AppDelegate : AppDelegateBase
{
    protected override void OnLaunch()
    {
        // Set 60FPS
        Application.targetFrameRate = 60;

#if DEBUG
        HiveFoundation.Instance.LogLevel = LogLevel.Debug;
#endif

        Appodeal.BannerShown += () => { Debug.Log("Banner shown"); };
        Appodeal.BannerContentChanged += () => { Debug.Log("Banner content changed"); };
        Appodeal.BannerClosed += () => { Debug.Log("Banner closed"); };

        Appodeal.Start();
    }
}
