﻿using Hive;
using UnityEngine;

public class WorldCameraSetup : MonoBehaviour
{
    protected virtual void Awake() { }

    private CameraClearFlags _cachedClearFlags;

    protected virtual void Start()
    {
        _cachedClearFlags = Game.Instance.WorldCamera.clearFlags;
        Game.Instance.WorldCamera.clearFlags = CameraClearFlags.Depth;
    }

    protected virtual void OnDestroy()
    {
        if (Game.Instance != null)
            Game.Instance.WorldCamera.clearFlags = _cachedClearFlags;
    }
}
